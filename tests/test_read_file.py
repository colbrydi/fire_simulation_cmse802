import pytest
import numpy as np  
import os
import scipy.io as si

def read_file(filename):
    '''
        Read input file with given name.

        Args:
            filename (string): full path for input file
        Returns:
            (numpy array): the content of the file
    '''
    if not os.path.isfile(filename):
        raise ValueError("Input file does not exist: {0}. I'll quit now.".format(filename))

    # code to load and parse the data from input file
    data = si.loadmat(filename)

    if not (len(data.keys())-3):
        # the data should not be empty then the dict should be have more than 3 keys
        raise ValueError("No data in input file.")

    return data
	
def test_file_load_good():
    # test if function returns data for good input file.
    data = read_file("./wind.mat")
    assert data

def test_file_load_bad():
    # Test if the function raises the exception if the input file is empty.
    with pytest.raises(ValueError) as excinfo:
        read_file("./empty.mat")

    assert "No data in input file" in str(excinfo.value)

def test_file_load_missing_file():
    # Test if the function raises the exception if nonexisting file name is passed as parameter.
    with pytest.raises(ValueError) as excinfo:
        read_file("non-existing_file.mat")

    assert "file does not exist" in str(excinfo.value)